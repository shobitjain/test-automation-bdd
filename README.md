# Test automation BDD

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Test automation to verify bank account validation functionality as BDD scenarios.

  - Makes an HTTP request 
  
  - Verifies the validation of bank account functionality based on the HTTP response following BDD

# Prerequisiste / Dependencies

Install the following software packages.

  - .Net core
  
  - SpecFlow
  
  - NUnit
  
  - RestSharp

### Running
In order to run, follow the instructions below.

  - Download and import the project (preferrably in Visual Studio), however any other IDE supporting microsoft .Net development can also be used.
  
  - Build the project (Solution)
  
  - Execute the tests (click on Run All in the Test Explorer). It should show that the test now passes (green).



