﻿Feature: Validation if bank account

@positive
Scenario: Positive
	Given a SampleRequest with a valid JWT token
	When sample request is posted to api
	Then Api returns ok

@negaive
Scenario: Negative
	Given a SampleRequest without a JWT token
	When sample request is posted to api
	Then Api returns „Authorization has been denied for this request.“ message.