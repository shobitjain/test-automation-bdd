﻿using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace assignmentArvato
{
    [Binding]
    public sealed class ValidateSteps
    {
        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        private readonly ScenarioContext _scenarioContext;

        public ValidateSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
        public string url = "https://api-test.afterpay.dev/api/v3/validate/bank-account";
        public object validResponse = new
        {
            isValid = true
        };
        public RestRequest request( string token)
        {
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Auth-Key", token);
            request.AddParameter("application/json", "{\r\n    \"bankAccount\": \"GB09HAOE91311808002317\"\r\n}\r\n", ParameterType.RequestBody);
            
            return request;
        }
        public RestClient client()
        {
            var client = new RestClient(url);
            client.Timeout = -1;
            return client;
        }
        [Given(@"a SampleRequest with a valid JWT token")]
        public void GivenASampleRequestWithAValidJWTToken()
        {
            request("Q7DaxRnFls6IpwSW1SQ2FaTFOf7UdReAFNoKY68L");
        }

        [When(@"sample request is posted to api")]
        public void WhenSampleRequestIsPostedToApi()
        {
            
            IRestResponse response = client().Execute(request("Q7DaxRnFls6IpwSW1SQ2FaTFOf7UdReAFNoKY68L"));
            //ScenarioContext.Current.Pending();
        }

        [Then(@"Api returns ok")]
        public void ThenApiReturnsOk()
        {
            
            IRestResponse response = client().Execute(request("Q7DaxRnFls6IpwSW1SQ2FaTFOf7UdReAFNoKY68L"));
            Console.WriteLine(response.Content);
            Assert.AreEqual("{\r\n  \"isValid\": true\r\n}", response.Content);
            //ScenarioContext.Current.Pending();
        }

        [Given(@"a SampleRequest without a JWT token")]
        public void GivenASampleRequestWithoutAJWTToken()
        {

            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            //ScenarioContext.Current.Pending();
        }

        [Then(@"Api returns „Authorization has been denied for this request\.“ message\.")]
        public void ThenApiReturnsAuthorizationHasBeenDeniedForThisRequest_Message_()
        {
            IRestResponse response = client().Execute(request(null));
            Console.WriteLine(response.Content);
            Assert.AreEqual("{\n    \"message\": \"Authorization has been denied for this request.\"\n}", response.Content);
            //ScenarioContext.Current.Pending();
        }



    }
}
